module.exports = {
  theme: {
    screens: {
      'xs': '480px',
      'sm': '640px',   
      'md': '768px',
      'lg': '1024px', 
      'xl': '1280px',   
      '2xl': '1440px',
    },
    extend: {
      colors: {
        black: '#3B3B3D',
      },
    },
    fontFamily: {
      sans: ['Helvetica', 'Arial', 'sans-serif'],
    }
  }
}