# ICE Solutions

## Requirements
- nodejs v12 and higher
- yarn

## Development

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev
```

## Production

### As a server

```bash
# build for production and launch server
$ yarn build
$ yarn start
```

### As a static site

```
# generate static project
$ yarn generate
```

now copy the `dist` folder to your server


## Directories
- Content: put your content here (markdown/json) and consume it in you page/component
- Layout: Base layout(s) for all pages (see it as a template)
- Components: stateless components that can be reused on multiple pages
- Pages: The pages them self
- Static: the statics (see it as your public folder)

## More info

- For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
- This project uses [tailwind](https://tailwindcss.com/) for styling the pages/components